<?php

namespace MercuryGateway;

require_once __DIR__ . '/../../../includes/gatewayfunctions.php';
require_once __DIR__ . '/mercury-cash-sdk/vendor/autoload.php';

use Exception;
//std - вроде пока мне не нужен
use stdClass;
use WHMCS\Database\Capsule;
use MercuryCash\SDK\Adapter;
use MercuryCash\SDK\Auth\APIKey;
use MercuryCash\SDK\Endpoints\Transaction;

class MercuryGateway
{
    // const MERCURYGATEWAYCONFIG = 'mercurygateway';
    //Mercury transaction statuses
    const PENDING = 'TRANSACTION_PENDING';
    const RECEIVED = 'TRANSACTION_RECEIVED';
    const APROVED = 'TRANSACTION_APROVED';


    private $version = '1.0';
    protected $baseApiUrl = 'https://api-way.mercurydev.tk';
    protected $currenceApiUrl = 'https://api.mercury.cash/api/price';
    protected $gatewayParams;
    protected $mercury_currencies_list;
    protected $availableFiatCurrencies = ['USD', 'EUR'];
    protected $userCurrency;

//'BTC' => 'Bitcoin',
//'ETH' =>'Etherium',
//'DASH' => 'Dash',

    public function __construct()
    {
        $this->gatewayParams = getGatewayVariables('mercurygateway');
    }

    /*
     * Get the mercury gateway version
     */
    public function getVersion()
    {
        return $this->version;
    }

    public function getBaseUrl()
    {
        return $this->baseApiUrl;
    }

    /*
     * Test mode switcher
     */
    public function isTestModeOn()
    {
        return $this->gatewayParams['testMode'];
    }

    /*
     * Get user configured API key from database
     */
    public function getPublicKey()
    {
        return $this->gatewayParams['publicKey'];
    }

    /*
     * Get user configured secret key from database
     */
    public function getSecretKey()
    {
        return $this->gatewayParams['secretKey'];
    }

    public function isSupportedCurrency($currencyCode)
    {
        return in_array($currencyCode, $this->availableFiatCurrencies);
    }


    /*
     * Get user configured Time Period from database
     */
    public function getTimePeriod()
    {
        $gatewayParams = getGatewayVariables('mercurygateway');
        return $this->gatewayParams['timePeriod'];
    }

    /*
     * Get URL of the WHMCS installation
     */
    public function getSystemUrl()
    {
        return Capsule::table('tblconfiguration')
            ->where('setting', 'SystemURL')
            ->value('value');
    }

    public function getLangFilePath($language = false)
    {
        if ($language && file_exists(dirname(__FILE__) . '/lang/' . $language . '.php')) {
            $langfilepath = dirname(__FILE__) . '/lang/' . $language . '.php';
        } else {
            global $CONFIG;
            $language = isset($CONFIG['Language']) ? $CONFIG['Language'] : '';
            $langfilepath = dirname(__FILE__) . '/lang/' . $language . '.php';
            if (!file_exists($langfilepath)) {
                $langfilepath = dirname(__FILE__) . '/lang/english.php';
            }
        }
        return $langfilepath;
    }


    /**
     * Create Mercury Transaction
     *
     */
    public function createTransaction($mail,$crypto,$fiat_currency,$amount){
        //fiat currency - так то by user, но если вдруг есть системная то можно тут поставить

        $api_key = new APIKey($this->getPublicKey(), $this->getSecretKey());
        $adapter = new Adapter($api_key, $this->getBaseUrl());
        $endpoint = new Transaction($adapter);

        $transaction = $endpoint->create([
            'email' => $mail,
            'crypto' => $crypto,
            'fiat' => $fiat_currency,
            'amount' => (float) $amount,
            'tip' => 0,
        ]);

        $endpoint->process($transaction->getUuid());

        return [
            'uuid' => $transaction->getUuid(),
            'cryptoAmount' => $transaction->getCryptoAmount(),
            'fiatIsoCode' => $transaction->getFiatIsoCode(),
            'fiatAmount' => $transaction->getFiatAmount(),
            'rate' => $transaction->getRate(),
            'address' => $transaction->getAddress(),
            'fee' => $transaction->getFee()
        ];
    }

    /**
     * Check Status Mercury Transaction
     *
     */
    public function checkStatus($uuid){

        $api_key = new APIKey($this->getPublicKey(), $this->getSecretKey());
        $adapter = new Adapter($api_key, $this->baseApiUrl);
        $endpoint = new Transaction($adapter);

        $status = $endpoint->status($uuid);

        return [
            'status' => $status->getStatus(),
        ];
    }

    /*
    * Get list of crypto currencies supported by Mercury
     *  надо будет эту проверку добавить в админ часть
     *  можно добавить чекбоксы - поддерживаемые криптовалюты

    /**
     * Get Mercury Currencies Available List
     * @return mixed
     */
    public function getMercuryCurrenceList(){
        //TODO replace with Guzle
        if(empty($this->mercury_currencies_list)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.mercury.cash/api/price');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($ch);
            curl_close($ch);


            $body = json_decode($response, true);

            $this->mercury_currencies_list = $body['data'];
        }
        return $this->mercury_currencies_list;
    }

}
//    public function setUserCurrency($userCurrency){
//        $this->userCurrency = $userCurrency;
//    }
//
//    public function getUserCurrency(){
//        return $this->userCurrency;
//    }
//
//
//
//    /** currency list prepared for frontend
//     *
//     * return currency list prepared for frontend
//     */
//    public function get_currency(){
////        if (empty( $this->userCurrency )){
////            $this->userCurrency = $this->getUserCurrency();
////        }
////        if  (!in_array($this->userCurrency,$this->availableFiatCurrencies)) {
////            //Проверить в админ части при активации модуля
////            return;
////        }
//        $this->userCurrency = 'USD';
//        // invoice amount in payment file
//        $data = $this->getMercuryCurrenceList();
//        $data =  $data[$this->userCurrency];
//
//        //in invoice
//        $order_price = 10;
//
//        foreach ($data as $key => $arr) {
//            if ($key == 'exchange') continue;
//            //!!! cart_amount - in invoice - in link function we have only and then at the payment page
//
//            $data[$key]['cart_amount'] = (float) $order_price;
//
//
//            //!!! min price for cryptos - in config get in link function
//
//            //        $this->BTC = $this->get_option( 'bitcoinmin' );
//            //        $this->ETH = $this->get_option( 'ethereummin' );
//            //        $this->DASH = $this->get_option( 'dashmin' );
////            $data[$key]['minprice'] = (float) $this->{$key};
//
//            // temp
//            $data[$key]['minprice'] = 3;
//            $data[$key]['shop_currency'] = $this->userCurrency;
//        }
//        return $data;
//
//    }
//
//    /** Тут должна быть функция сверки системной валюты и доступных валют
//     *
//     */
////    public function check_current_cur( $gateways ) {
////        $current_currency = get_option('woocommerce_currency');
////        $data = $this->getMercuryCurrenceList();
////        if ( isset( $gateways['mercury'] ) &&  !array_key_exists($current_currency, $data)) {
////            unset( $gateways['mercury'] );
////        }
////        return $gateways;
////    }
//
//    /* DEPRECATED
//     * Get list of active crypto currencies
//     */
//    public function getActiveCurrencies() {
//        $active_currencies = array();
//        $blockonomics_currencies = $this->getSupportedCurrencies();
//        foreach ($blockonomics_currencies as $code => $currency) {
//            $enabled = true;
//            // так надо еще подумать - вывести ли в настройки доступные валюты
////            if($code == 'btc'){
////                $enabled = true;
////            }else{
////                $gatewayParams = getGatewayVariables('blockonomics');
////                $enabled = $gatewayParams[$code.'Enabled'];
////            }
//            if($enabled){
//                $active_currencies[$code] = $currency;
//            }
//        }
//        return $active_currencies;
//    }
//
//
//
//    ////////////////////////////////////
//    /// ORDER HASH  - temprorary order id
//    ///
//    ///
//
//    // Try with order HASH for test only
//    /*
//     * Add a new skeleton order in the db
//     */
//    public function getOrderHash($id_order, $amount, $currency)
//    {
//        return $id_order;
//       // return $id_order . ":" . $amount . ":" . $currency;
//    }
//
//     /**
//     * Get the order id using the order hash
//     */
//    public function getOrderIdByHash($id_order)
//    {
//        return $id_order;
////        $order_info = $this->decryptHash($order_hash);
////        return $order_info->id_order;
//    }
//
//    ///////////////////////////////
//    ///
//
//    /**
//        DB
//     * DataBase functions
//     */
//
//    /*
// * If no Mercury order table exists, create it
// */
//    public function createOrderTableIfNotExist() {
//
//        if (!Capsule::schema()->hasTable('mercury_orders')) {
//
//            /**
//             * /**
//             * class MercuryCash\SDK\Responses\Checkout#19 (3) {
//            protected $uuid =>
//            string(36) "506c7092-018b-42fd-aa8b-9f3ba1379c0f"
//            protected $address =>
//            string(42) "tb1qfqttg66pw9me2e0cd0kv72evvyxpqf2jk99r6j"
//            protected $networkFee =>
//            double(1.0E-5)
//            }
//
//             */
//
//
//            /**
//             * class MercuryCash\SDK\Responses\Status#33 (8) {
//            protected $uuid =>
//            string(36) "36554e25-68dd-4402-a949-95abb01581e2"
//            protected $fromAddress =>
//            string(42) "0xb00De8c00CaFE72FCde54302Bec6EcD16984EEd9"
//            protected $amount =>
//            int(0)
//            protected $currency =>
//            string(3) "ETH"
//            protected $transactionFee =>
//            int(0)
//            protected $networkFee =>
//            int(0)
//            protected $user =>
//            string(36) "f41eaa30-557f-11eb-b738-2b7701f4917c"
//            protected $status =>
//            string(19) "TRANSACTION_PENDING"
//             *
//             * const PENDING = 'TRANSACTION_PENDING';
//            const RECEIVED = 'TRANSACTION_RECEIVED';
//            const APROVED = 'TRANSACTION_APROVED';
//             *
//             *
//             *
//             */
//
//
//            try {
//                Capsule::schema()->create( 'mercury_orders', function ($table) {
//                    $table->integer('id_order');
//                    $table->string('uuid');
//                    $table->string('address');
//
//                    $table->string('currency');
//                    $table->string('crypto_currency');
//                    $table->string('status');
//
//                    $table->integer('transactionFee');
//                    $table->integer('networkFee');
//
//                    $table->integer('timestamp');
//
//
//                    $table->primary('uuid');
//                    $table->index('id_order');
//                }
//                );
//            } catch (Exception $e) {
//                exit("Unable to create mercury_orders: {$e->getMessage()}");
//            }
//        }
//    }
//
//    /*
//     * Update existing order information. Use BTC payment address as key
//     * надо или по uuuid или по order_id искать будет
//     *
//     */
//    public function updateOrderInDb($addr, $uuid, $status) {
//        try {
//            Capsule::table('blockonomics_orders')
//                ->where('addr', $addr)
//                ->update([
//                        'uuid' => $uuid,
//                        'status' => $status,
//                        //'bits_payed' => $bits_payed
//                    ]
//                );
//        } catch (Exception $e) {
//            exit("Unable to update order to blockonomics_orders: {$e->getMessage()}");
//        }
//    }
//
////    /*
////     * Update existing order's expected amount and FIAT amount. Use WHMCS invoice id as key
////     */
////    public function updateOrderExpected($address, $blockonomics_currency, $timestamp, $value, $bits) {
////        try {
////            Capsule::table('blockonomics_orders')
////                ->where('addr', $address)
////                ->update([
////                        'blockonomics_currency' => $blockonomics_currency,
////                        'value' => $value,
////                        'bits' => $bits,
////                        'timestamp' => $timestamp
////                    ]
////                );
////        } catch (Exception $e) {
////            exit("Unable to update order to blockonomics_orders: {$e->getMessage()}");
////        }
////    }
//
//
//    //////////////////////////////////////////////////////////////
//
//}
//
