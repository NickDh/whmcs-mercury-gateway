<?php
if (!defined("WHMCS")) die("This file cannot be accessed directly");

// Module meta data

// WHMCS Module name
$_MERCURYLANG['module']['title'] = 'Mercury';
// WHMCS Module version
$_MERCURYLANG['version']['title'] = 'Module version';

//Config pages

$_MERCURYLANG['publicKey']['title'] = "Public Key";
$_MERCURYLANG['publicKey']['description'] = 'Enter your public key here';

$_MERCURYLANG['secretKey']['title'] = "Secret Key";
$_MERCURYLANG['secretKey']['description'] = 'Enter secret key here';

$_MERCURYLANG['timePeriod']['title'] = 'Time period';
$_MERCURYLANG['timePeriod']['description'] = 'time in minutes for waiting payment';


// Checkout pages
$_MERCURYLANG['orderId'] = "Order #";

$_MERCURYLANG['error']['common']['title'] = "Mercury Common Error";


//
$_MERCURYLANG['error']['api']['title'] = "Could not connect to Mercury API";

// error message about test setup - maybe add test btn at the end
$_MERCURYLANG['error']['api']['message'] = "Note to webmaster: Please login to admin and go to Setup > Payments > Payment Gateways > Manage Existing Gateways and use the Test Setup button to diagnose the error.";


// errors
$_MERCURYLANG['error']['btc']['title'] = "Could not generate new Bitcoin address.";
$_MERCURYLANG['error']['btc']['message'] = "Note to webmaster: Please login to admin and go to Setup > Payments > Payment Gateways > Manage Existing Gateways and use the Test Setup button to diagnose the error.";
$_MERCURYLANG['error']['bch']['title'] = "Could not generate new Bitcoin Cash address.";
$_MERCURYLANG['error']['bch']['message'] = "Note to webmaster: Please follow the instructions <a href=\"https://help.blockonomics.co/support/solutions/articles/33000251576-bch-setup-on-whmcs\" target=\"_blank\">here</a> to configure BCH payments.";
$_MERCURYLANG['error']['pending']['title'] = "Payment is pending";
$_MERCURYLANG['error']['pending']['message'] = "Additional payments to invoice are only allowed after current pending transaction is confirmed.";



$_MERCURYLANG['payWith'] = "Pay With";
$_MERCURYLANG['paymentExpired'] = "Payment Expired";
$_MERCURYLANG['tryAgain'] = "Click here to try again";
$_MERCURYLANG['paymentError'] = "Payment Error";
$_MERCURYLANG['openWallet'] = "Open in wallet";
$_MERCURYLANG['payAmount'] = "To pay, send exactly this [[currency.code | uppercase]] amount";
$_MERCURYLANG['payAddress'] = "To this [[currency.name | lowercase]] address";
$_MERCURYLANG['copyClipboard'] = "Copied to clipboard";
$_MERCURYLANG['howToPay'] = "How do I pay?";
$_MERCURYLANG['poweredBy'] = "Powered by Blockonomics";

// delete - only for test
//////////////////////////////////////////////////////////////////////////////////////////////
// Callback
$_MERCURYLANG['error']['secret'] = "Secret verification failure";
$_MERCURYLANG['invoiceNote']['waiting'] = "Waiting for Confirmation on";
$_MERCURYLANG['invoiceNote']['network'] = "network";

