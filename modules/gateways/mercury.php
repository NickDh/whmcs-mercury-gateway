<?php

require_once(dirname(__FILE__) . '/mercury/mercury.php');


use Mercury\Mercury;

/**
 * WHMCS Sample Payment Gateway Module
 *
 * Payment Gateway modules allow you to integrate payment solutions with the
 * WHMCS platform.
 *
 * This sample file demonstrates how a payment gateway module for WHMCS should
 * be structured and all supported functionality it can contain.
 *
 * Within the module itself, all functions must be prefixed with the module
 * filename, followed by an underscore, and then the function name. For this
 * example file, the filename is "gatewaymodule" and therefore all functions
 * begin "gatewaymodule_".
 *
 * If your module or third party API does not support a given function, you
 * should not define that function within your module. Only the _config
 * function is required.
 *
 * For more information, please refer to the online documentation.
 *
 * @see https://developers.whmcs.com/payment-gateways/
 *
 * @copyright Copyright (c) WHMCS Limited 2017
 * @license http://www.whmcs.com/license/ WHMCS Eula
 */

if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

/**
 * Define module related meta data.
 *
 * Values returned here are used to determine module related capabilities and
 * settings.
 *
 * @see https://developers.whmcs.com/payment-gateways/meta-data-params/
 *
 * @return array
 */
function mercury_MetaData()
{
    return array(
        'DisplayName' => 'Mercury Module',
        'APIVersion' => '1.1', // Use API Version 1.1
        'DisableLocalCreditCardInput' => true,
        'TokenisedStorage' => false,
    );
}

/**
 * Define gateway configuration options.
 *
 * The fields you define here determine the configuration options that are
 * presented to administrator users when activating and configuring your
 * payment gateway module for use.
 *
 * Supported field types include:
 * * text
 * * password
 * * yesno
 * * dropdown
 * * radio
 * * textarea
 *
 * Examples of each field type and their possible configuration parameters are
 * provided in the sample function below.
 *
 * @return array
 */
function mercury_config()
{
    $mercury = new Mercury();
    require($mercury->getLangFilePath());

    $settings_array = array(
        // the friendly display name for a payment gateway should be
        // defined here for backwards compatibility
        'FriendlyName' => array(
            'Type' => 'System',
            'Value' => $_MERCURYLANG['module']['title'],
        ),
        array(
            'FriendlyName' => '<span style="color:grey;">'.$_MERCURYLANG['version']['title'].'</span>',
            'Description'  => '<span style="color:grey;">'.$mercury->getVersion().'</span>'
        ),
        // a text field type allows for single line text input
        'publicKey' => array(
            'FriendlyName' => $_MERCURYLANG['publicKey']['title'],
            'Description'  => $_MERCURYLANG['publicKey']['description'],
            'Type' => 'text',
            'Default' => '',
        ),
        // a password field type allows for masked text input
        'secretKey' => array(
            'FriendlyName' => $_MERCURYLANG['secretKey']['title'],
            'Description'  => $_MERCURYLANG['secretKey']['description'],
            'Type' => 'password',
            'Default' => '',
        ),

        // time in minutes for waiting transaction
        'timePeriod' => array(
            'FriendlyName' => $_MERCURYLANG['timePeriod']['title'],
            'Description'  => $_MERCURYLANG['timePeriod']['description'],
            'Type' => 'text',
            'Default' => '30',
        ),
        // time in minutes for waiting transaction
        'minBTC' => array(
            'FriendlyName' => 'min Btc',
            'Description'  => 'minumum for BTC transaction',
            'Type' => 'text',
            'Default' => '9',
        ),
        // time in minutes for waiting transaction
        'minETH' => array(
            'FriendlyName' => 'min ETH',
            'Description'  => 'minumum for ETH transaction',
            'Type' => 'text',
            'Default' => '9',
        ),
        // time in minutes for waiting transaction
        'minDASH' => array(
            'FriendlyName' => 'min DASH',
            'Description'  => 'minumum for DASH transaction',
            'Type' => 'text',
            'Default' => '9',
        ),
        // a password field type allows for masked text input
        'testMode' => array(
            'FriendlyName' => 'Test mode on',
            'Description'  => 'Enable test mode',
            'Type' => 'yesno',
            'Default' => '',
        ),
        // a text field type allows for single line text input
        'publicKeyTest' => array(
            'FriendlyName' => $_MERCURYLANG['publicKey']['title'],
            'Description'  => $_MERCURYLANG['publicKey']['description'] . 'for Test',
            'Type' => 'text',
            'Default' => '',
        ),
        // a password field type allows for masked text input
        'secretKeyTest' => array(
            'FriendlyName' => $_MERCURYLANG['secretKey']['title'],
            'Description'  => $_MERCURYLANG['secretKey']['description']. 'for Test',
            'Type' => 'password',
            'Default' => '',
        ),
    );

    return $settings_array;
}

///**
// * Payment link.
// *
// * Required by third party payment gateway modules only.
// *
// * Defines the HTML output displayed on an invoice. Typically consists of an
// * HTML form that will take the user to the payment gateway endpoint.
// *
// * @param array $params Payment Gateway Module Parameters
// *
// * @return string
// * @see https://developers.whmcs.com/payment-gateways/third-party-gateway/
// *
// */
//function mercury_link($params)
//{
//
//    if (false === isset($params) || true === empty($params)) {
//        die('[ERROR] In modules/gateways/mercurygateway.php::mercurygateway_link() function: Missing or invalid $params data.');
//    }
//
//    $mercury = new Mercury();
//    $system_url = $mercury->getSystemUrl();
//
////
////    //надо найти открытые ордера на этот инвойс - вдруг уже создали ордер и ждем подтверждения
////    $order_hash = $mercury->getOrderHash($params['invoiceid'], $params['amount'], $params['currency']);
////
//    $form_url = $system_url . 'mercurypayment.php';
//
//    //pass only the uuid to the payment page
//    $form = '<form action="' . $form_url . '" method="GET">';
////    $form .= '<input type="hidden" name="order" value="' . $order_hash . '"/>';
//    $form .= '<input type="hidden" name="invoice_currency" value="' . $params['currency'] . '"/>';
//    $form .= '<input type="hidden" name="order_amount" value="' . $params['amount'] . '"/>';
//
//    $form .= '<input type="hidden" name="billing_email" value="' . $params['clientdetails']['email'] . '"/>';
//
//    $form .= '<input type="submit" value="' . $params['langpaynow'] . '"/>';
//    $form .= '</form>';
//
//    return $form;
//}