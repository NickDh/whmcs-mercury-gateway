<?php

require_once(dirname(__FILE__) . '/modules/gateways/mercury/mercury.php');
//
use WHMCS\ClientArea;
//use WHMCS\Database\Capsule;
use Mercury\Mercury;
//use Exception;

define('CLIENTAREA', true);
require 'init.php';

// Init Mercury class and language file.
$mercury = new Mercury();
require($mercury->getLangFilePath(isset($_REQUEST['language']) ? $_REQUEST['language'] : ""));

$ca = new ClientArea();
$ca->setPageTitle('Mercury Payment');
$ca->addToBreadCrumb('index.php', Lang::trans('globalsystemname'));
// check in docs
$ca->addToBreadCrumb('mercurypayment.php', 'Mercury Payment');
$ca->initPage();

//$currentUser = new \WHMCS\Authentication\CurrentUser;
//$user = $currentUser->user();
//if (!$user) {
//    //add to language file
//    echo "There is not an authenticated User.";
//    exit();
//}

//$currencyData = getCurrency($user->ID);
//$currency = $currencyData['code'];
//// $mercury->setUserCurrency($currency);
//if (!$mercury->isSupportedCurrency($currency)){
//    echo "Currency is not supported.";
//    exit();
//}
//$ca->assign('currency', $currency);


//$system_url = $mercury->getSystemUrl();
//$ca->assign('system_url', $system_url);

/*
 * SET POST PARAMETERS TO VARIABLES AND CHECK IF THEY EXIST
 */

// Ajax flags - boolean
//$ajaxCreateTransaction = htmlspecialchars(isset($_REQUEST['ajax_create_transaction']) ? $_REQUEST['ajax_create_transaction'] : "");
//$ajaxCheckTransaction = htmlspecialchars(isset($_REQUEST['ajax_check_transaction']) ? $_REQUEST['ajax_check_transaction'] : "");
//
//// check in Mercury - get_order - chould contain order from DB with transaction details
//$get_order = htmlspecialchars(isset($_REQUEST['get_order']) ? $_REQUEST['get_order'] : "");
//$finish_order = htmlspecialchars(isset($_REQUEST['finish_order']) ? $_REQUEST['finish_order'] : "");
//$order_hash = htmlspecialchars(isset($_REQUEST['order']) ? $_REQUEST['order'] : "");
//
////
//$order_amount = htmlspecialchars(isset($_REQUEST['order_amount']) ? $_REQUEST['order_amount'] : "");
////billing email
//$billing_email = htmlspecialchars(isset($_REQUEST['billing_email']) ? $_REQUEST['billing_email'] : "");



// В первый раз посылаем на эту форму только инвойс с выбором крипты

// Во второй раз шлем сюда же тот же инвойс, только с выбранной валютой

// После того как послали транзакцию на меркури - проверяем ее статус.

// test ajax
//if ($ajaxCreateTransaction){
//    $mail = htmlspecialchars(isset($_REQUEST['mail']) ? $_REQUEST['mail'] : "");
//    $crypto = htmlspecialchars(isset($_REQUEST['crypto']) ? $_REQUEST['crypto'] : "");
//    $order_amount = htmlspecialchars(isset($_REQUEST['order_amount']) ? $_REQUEST['order_amount'] : "");
//    $currency = htmlspecialchars(isset($_REQUEST['currency']) ? $_REQUEST['currency'] : "");
//
//    try {
//        $transactionData = $mercury->createTransaction($mail,$crypto,$currency,$order_amount);
//    }catch (\Exception $exception){
//        header("Content-Type: application/json");
//        exit(json_encode(['status' => 'failed','error'=>'Cant create transaction']));
//    }
//
//    //logModuleCall('Mercury', 'mercuryTransactionCreated', $mail.$crypto.$currency.$order_amount, $transactionData, $transactionData, $transactionData);
//
//   // $command = 'CreateTransaction';
//    // $adminUsername = getAdminUserName();
//
//    $postData = array(
//        'status' => 'success',
//        'data' => $transactionData,
//    );
//
//    header("Content-Type: application/json");
//    exit(json_encode($postData));
//}
//
//// test ajax
//if ($ajaxCheckTransaction){
////    $mail = htmlspecialchars(isset($_REQUEST['mail']) ? $_REQUEST['mail'] : "");
////    $crypto = htmlspecialchars(isset($_REQUEST['crypto']) ? $_REQUEST['crypto'] : "");
////    $order_amount = htmlspecialchars(isset($_REQUEST['order_amount']) ? $_REQUEST['order_amount'] : "");
////    $currency = htmlspecialchars(isset($_REQUEST['currency']) ? $_REQUEST['currency'] : "");
////
////    try {
////        $transactionData = $mercury->createTransaction($mail,$crypto,$currency,$order_amount);
////    }catch (\Exception $exception){
////        header("Content-Type: application/json");
////        exit(json_encode(['status' => 'failed','error'=>'Cant create transaction']));
////    }
////
////    //logModuleCall('Mercury', 'mercuryTransactionCreated', $mail.$crypto.$currency.$order_amount, $transactionData, $transactionData, $transactionData);
////
////    // $command = 'CreateTransaction';
////    // $adminUsername = getAdminUserName();
////
////    $postData = array(
////        'status' => 'success',
////        'data' => $transactionData,
////    );
////
////    header("Content-Type: application/json");
////    exit(json_encode($postData));
//}

//if($get_order){
//    $existing_order = $mercury->processOrderHash($get_order, $mercury_currency);
//    // No order exists, exit
//    if(is_null($existing_order->id_order)) {
//        exit;
//    } else {
//        header("Content-Type: application/json");
//        exit(json_encode($existing_order));
//    }
//}else if($finish_order){
//    $finish_url = $system_url . 'viewinvoice.php?id=' . $finish_order . '&paymentsuccess=true';
//    header("Location: $finish_url");
//    exit();
//}else if(!$order_hash) {
//    echo "<b>Error: Failed to fetch order data.</b> <br>
//				Note to admin: Please check that your System URL is configured correctly.
//				If you are using SSL, verify that System URL is set to use HTTPS and not HTTP. <br>
//				To configure System URL, please go to WHMCS admin > Setup > General Settings > General";
//    exit;
//}
//
//$ca->assign('order_uuid', $order_hash);
//
//$time_period_from_db = $mercury->getTimePeriod();
//$time_period = isset($time_period_from_db) ? $time_period_from_db : '10';
//$ca->assign('time_period', $time_period);
//
//$active_crypto_currencies = $mercury->get_currency();
//
//if ($active_crypto_currencies) {
//    $ca->assign('active_crypto_currencies', json_encode($active_crypto_currencies));
//    $ca->assign('currency', $currency);
//    $ca->assign('order_amount', $order_amount);
//
//    $ca->assign('user', json_encode($user));
//    $ca->assign('billing_email', $billing_email);
//    }else{
//    echo "<b>Error: No active mercury crypto currencies.</b> <br>
//				Note to admin: Service unavailable.";
//    exit;
//}

//$order_id = $mercury->getOrderIdByHash($order_hash);
//
//$ca->assign('order_id', $order_id);


$ca->assign('_MERCURYLANG', $_MERCURYLANG);

# Define the template filename to be used without the .tpl extension
$ca->setTemplate('../mercury/payment.tpl');


$ca->output();

?>
